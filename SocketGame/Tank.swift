//
//  Tank.swift
//  SocketGame
//
//  Created by Alex Kuzovkov on 12.10.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import SpriteKit

enum TankColor: String {
    case Beige = "Beige"
    case Blue = "Blue"
    case Green = "Green"
    case Red = "Red"
}
class Tank: SKSpriteNode {
    var clientid = "0"
    var barrel:SKSpriteNode?

    init(color:TankColor) {
        let texture = SKTexture(imageNamed:"tank\(color.rawValue)_outline.png")
        super.init(texture: texture, color: .clear, size: texture.size())
        
        barrel = SKSpriteNode(imageNamed: "barrel\(color.rawValue)_outline.png")
        barrel?.anchorPoint.y = 0.0
        barrel?.anchorPoint.x = 0.5
        self.addChild(barrel!)
    }
    
    init(color:String) {
        let texture = SKTexture(imageNamed:"tank\(color)_outline.png")
        super.init(texture: texture, color: .clear, size: texture.size())
        
        self.zPosition = 2
        
        barrel = SKSpriteNode(imageNamed: "barrel\(color)_outline.png")
        barrel?.anchorPoint.y = 0.0
        barrel?.anchorPoint.x = 0.5
        barrel?.zPosition = 3
        self.addChild(barrel!)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
