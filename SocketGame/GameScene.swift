//
//  GameScene.swift
//  SocketGame
//
//  Created by Alex Kuzovkov on 11.10.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import SpriteKit
import GameplayKit

import SocketIO

enum SocketRoute: String {
    case MAP                = "MAP"
    case TANK               = "TANK"
    case REMOVE_TANK        = "REMOVE_TANK"
    case MY_TANK_ID         = "MY_TANK_ID"
    
    case SANDBAG            = "SANDBAG"
    case REMOVE_SANDBAG     = "REMOVE_SANDBAG"
    
    case SHOOT              = "SHOOT"
    case BULLET_DESTROY     = "BULLET_DESTROY"
    
    //outbound
    
    case TANK_MOVE          = "TANK_MOVE"
    case TANK_STOP          = "TANK_STOP"
    
    case BARREL_MOVE        = "BARREL_MOVE"
    case BARREL_STOP        = "BARREL_STOP"
}

class GameScene: SKScene {
    
    private var tanks = [Tank]()
    private var bullets = [Bullet]()
    private var sandbags = [SKSpriteNode]()
    
    var mainCamera: SKCameraNode?
    
    var myTank:Tank?
    
    let socket = SocketIOClient(socketURL: URL(string: "http://192.168.1.37:3001")!, config: [.log(false), .compress])
    
    private var touchLocationX:CGFloat = 0.0
    private var touchLocationY:CGFloat = 0.0
    private var isTouching = false
    
    override func didMove(to view: SKView) {
        
        mainCamera = self.childNode(withName: "Camera") as? SKCameraNode!
        //Инциализация прослушки событий
        initSocket()
        //Подключение к сокету
        socket.connect()
        
        //Инициализация джойстиков
        initJoystick()
    }

    func initSocket() {
        //Событие при подсоединении
        socket.on(clientEvent: .connect) {data, ack in
            print("connected")
            for tank in self.tanks  {
                tank.removeFromParent()
                self.tanks.remove(at: self.tanks.index(of: tank)!)
            }
        }
        
        //Событие при отсоединении
        socket.on(clientEvent: .disconnect) {data, ack in
            self.socket.connect()
        }
        
        
        socket.on(SocketRoute.MAP.rawValue) {data, ack in
            for (i,layerX) in (data[0] as! NSArray).reversed().enumerated() {
                for (j,tileName) in (layerX as! NSArray).enumerated() {
                    let tName = tileName as! String
                    let groundSprite = SKSpriteNode(imageNamed: "\(tName).png")
                    groundSprite.position.x = 128*CGFloat(j)
                    groundSprite.position.y = 128*CGFloat(i)
                    groundSprite.zPosition = 1
                    self.addChild(groundSprite)
                }
            }
        }
        
        socket.on(SocketRoute.SANDBAG.rawValue) {data, ack in
            let barrelData = data[0] as? NSDictionary
            let x = barrelData?["x"] as! CGFloat
            let y = barrelData?["y"] as! CGFloat
            let rotation = (barrelData?["rotation"] as! CGFloat)
            
            let sandbag = SKSpriteNode(imageNamed: "sandbagBeige")
            sandbag.position.x = x
            sandbag.position.y = y
            sandbag.zRotation = rotation
            sandbag.zPosition = 5
            self.addChild(sandbag)
            
            self.sandbags.append(sandbag)

        }
        
        socket.on(SocketRoute.MY_TANK_ID.rawValue) {data,ack in
            let tankData = data[0] as? NSDictionary
            let clientid = tankData?["id"] as! String
            
            for tank in self.tanks {
                if tank.clientid==clientid {
                    self.myTank = tank
                }
            }

        }
        
        socket.on(SocketRoute.REMOVE_SANDBAG.rawValue) {data, ack in
            let bagData = data[0] as? NSDictionary
            let index = bagData?["index"] as! Int
            
            self.sandbags[index].removeFromParent()
            self.sandbags.remove(at: index)
        }
        
        //Событие по обновлению информации по танку
        socket.on(SocketRoute.TANK.rawValue) {data, ack in
            //Достаем информацию о танке из пришедших данных
            let tankData = data[0] as? NSDictionary
            let clientid = tankData?["clientid"] as! String
            let tankColor = tankData?["color"] as! String
            let x = tankData?["x"] as! CGFloat
            let y = tankData?["y"] as! CGFloat
            let rotation = (tankData?["rotation"] as! CGFloat)
            let barrelRotation = (tankData?["barrelRotation"] as! CGFloat)
            
            //Проверяем, знаем ли мы уже об этом танке
            //Цикл по массиву с танками self.tank, сравниваем clientid с пришедшим
            
            var tankExists = false
            for tank in self.tanks {
                if tank.clientid==clientid {
                    //Нашли танк в массиве
                    //Меняем его параметры на новые
                    tankExists = true
                    
                    let moveAction = SKAction.move(to: CGPoint(x:x, y:y), duration: TimeInterval(0.1))
                    tank.run(moveAction)
                    
                    tank.zRotation = rotation
                    tank.barrel?.zRotation = barrelRotation
                }
            }
            
            //Мы ничего не знаем про пришедший танк, надо его создать
            if !tankExists {
                let tank = Tank(color: tankColor)
                
                tank.clientid = clientid
                tank.position.x = x
                tank.position.y = y
                tank.zRotation = rotation
                tank.zPosition = 2
                tank.barrel?.zRotation = barrelRotation
                
                
                self.addChild(tank)
                self.tanks.append(tank)
            }
        }
        
        //Событие по которому танк нужно убрать со сцены
        //Сейчас это происходит по дисконнекту пользователя, которому принадлежал этот танк
        socket.on(SocketRoute.REMOVE_TANK.rawValue) {data, ack in
            let tankData = data[0] as? NSDictionary
            let clientid = tankData?["clientid"] as! String
            for tank in self.tanks  {
                if tank.clientid==clientid {
                    tank.removeFromParent()
                    self.tanks.remove(at: self.tanks.index(of: tank)!)
                    break;
                }
            }
        }
        
        
        socket.on(SocketRoute.SHOOT.rawValue) {data, ack in
            let bulletData = data[0] as? NSDictionary
            
            let bullet = Bullet(imageNamed: "bulletBeige_outline")
            bullet.position.x = bulletData?["x"] as! CGFloat
            bullet.position.y = bulletData?["y"] as! CGFloat
            bullet.zRotation = bulletData?["rotation"] as! CGFloat
            bullet.zPosition = 3
            
            bullet.anchorPoint.y = 0.5
            
            bullet.bulletID = bulletData?["id"] as! Int
            
            let pixelsPerSecond = bulletData?["pixelsPerSecond"] as! CGFloat
            
            self.bullets.append(bullet)
            self.addChild(bullet)
            
            let bulletMove = SKAction.move(by: CGVector.init(dx: pixelsPerSecond*cos(bullet.zRotation + CGFloat(Double.pi/2)),
                                                             dy: pixelsPerSecond*sin(bullet.zRotation+CGFloat(Double.pi/2))),
                                                        duration: TimeInterval(1))
            bullet.run(SKAction.repeatForever(bulletMove))
            
            
            let smoke = SKSpriteNode(imageNamed: "smokeWhite0")
            smoke.position.x = bullet.position.x
            smoke.position.y = bullet.position.y
            smoke.zRotation = bullet.zRotation
            smoke.anchorPoint.y = 0
            smoke.zPosition = 5
            self.addChild(smoke)
            smoke.run(SKAction.scale(to: 2.0, duration: 1.0))
            smoke.run(
                SKAction.sequence([
                    SKAction.fadeAlpha(to: 0, duration: 1.0),
                    SKAction.removeFromParent()
                    ]))
            
           
        
        }
        
        socket.on(SocketRoute.BULLET_DESTROY.rawValue) {data, ack in
            let bulletData = data[0] as? NSDictionary
            let bulletID = bulletData?["id"] as! Int
            
            for bullet in self.bullets {
                if bullet.bulletID==bulletID {
                    print("\(bullet.position.x) \(bullet.position.y)")
                    bullet.removeFromParent()
                    self.bullets.remove(at: self.bullets.index(of: bullet)!)
                    
                    let smoke = SKSpriteNode(imageNamed: "smokeOrange3")
                    smoke.position.x = bullet.position.x
                    smoke.position.y = bullet.position.y
                    smoke.zPosition = 5
                    self.addChild(smoke)
                    smoke.run(SKAction.scale(to: 2.0, duration: 0.5))
                    smoke.run(
                        SKAction.sequence([
                            SKAction.fadeAlpha(to: 0, duration: 0.5),
                            SKAction.removeFromParent()
                            ]))
                    
                }
            }
        }
    }
    
    func initJoystick() {
        let joystick = AnalogJoystick(diameter: 100, colors: (.blue, .yellow))
        joystick.position.x = -self.frame.size.width/2+200
        joystick.position.y = -self.frame.size.height/2+200
        joystick.zPosition = 10
        
        self.mainCamera!.addChild(joystick)
        
        var moveAngle = CGFloat(0)
        var beginMoving = false
        
        joystick.beginHandler = { [unowned self] in
            beginMoving = true
        }
        //Отправляем данные по джойстику, когда мы его теребим. Отправляем только угол в радианах.
        joystick.trackingHandler = { [unowned self] data in
            if (beginMoving || !(moveAngle==data.angular)) {
                beginMoving = false
                moveAngle = data.angular
                self.socket.emit(SocketRoute.TANK_MOVE.rawValue, ["angle":data.angular])
            }
        }
        joystick.stopHandler = { [unowned self] in
            beginMoving = false
            self.socket.emit(SocketRoute.TANK_STOP.rawValue, [])
        }
        
        
        
        let barrelJoystick = AnalogJoystick(diameter: 100, colors: (.blue, .yellow))
        barrelJoystick.position.x = self.frame.size.width/2-200
        barrelJoystick.position.y = -self.frame.size.height/2+200
        barrelJoystick.zPosition = 10
        
        self.mainCamera!.addChild(barrelJoystick)
        
        barrelJoystick.beginHandler = { [unowned self] in
            
        }
        
        barrelJoystick.trackingHandler = { [unowned self] data in
            if (abs(data.velocity.x)>15 || abs(data.velocity.y)>15) {
                self.socket.emit(SocketRoute.BARREL_MOVE.rawValue, ["angle":data.angular])
            }
        }
        
        barrelJoystick.stopHandler = { [unowned self] in
            self.socket.emit(SocketRoute.BARREL_STOP.rawValue, [])
        }


    }
    
    override func update(_ currentTime: TimeInterval) {
        if (myTank != nil) {
            var dirVector = CGVector.init(dx: myTank!.position.x-self.mainCamera!.position.x, dy: myTank!.position.y-self.mainCamera!.position.y)
            dirVector = dirVector.normalize()
            
            let targetVector = CGVector.init(dx: myTank!.position.x, dy: myTank!.position.y)
            
            let distance = CGVector.init(dx: self.mainCamera!.position.x, dy: self.mainCamera!.position.y).distanceTo(targetVector)
            
            if (distance>0) {
                dirVector*=(distance*0.1)
                
                self.mainCamera!.position.x+=dirVector.dx
                self.mainCamera!.position.y+=dirVector.dy
            }
            
        }
    }
}
